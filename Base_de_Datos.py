import sqlite3


class BD:
    nombre = ''
    email = ''
    carrera = ''
    ciclo = int
    cedula = int
    id = int
    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        print("Conexion exitosa !!!")

    def crear_bd(self):
        sql_academia = """CREATE TABLE academia (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, Cedula INTEGER, 
        Nombre   TEXT UNIQUE, 
        Email  TEXT, Carrera TEXTO, Ciclo INTEGER)"""

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_academia)
        print("Tablas creadas  !!!")
        self.cursor.close()


    def Matric_est(self,cedula,nombre, email, carrera, ciclo):
        self.cedula = cedula
        self.nombre = nombre
        self.email = email
        self.carrera = carrera
        self.ciclo = ciclo
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO academia(Cedula, Nombre, Email, Carrera, Ciclo) VALUES(?,?,?,?,?)",
                                [(self.cedula,self.nombre,self.email,self.carrera,self.ciclo)])
        self.conexion.commit()
        self.cursor.close()
        print ("Estudiantes matriculados")

    def visualizar_datos(self,nombre):
        self.nombre = nombre
        sql = "SELECT * FROM academia  WHERE cedula =" + self.nombre
        self.cursor = self.conexion.cursor()

        filas = self.cursor.execute(sql)
        for f in filas:
            print(f)
        self.cursor.close()

    def buscar_datos(self,id):
        self.id = id
        sql = "SELECT * FROM academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        filas = self.cursor.execute(sql)
        for f in filas:
            print(f[2])
        self.cursor.close()

    def eliminar_datos(self,id):
        self.id = id
        sql = "DELETE FROM academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        self.conexion.commit()
        self.cursor.close()


if __name__ == "__main__":
    base = BD()
    #base.crear_bd()
    print ('\t\t\tBIENVENIDO \n'
           'Opciones disponibles en la base de datos academia:\n'
           '1.Matricular estudiante.\n'
           '2.Visualizar datos del estudiante\n'
           '3.Buscar estudiante por ID\n'
           '4.Eliminar estudiante por ID\n')
    while True:
        op = int(input('¿Ingresa una opcion ? (1,2,3,4) \n'))
        if op == 1:
            cedula = int(input ('Ingrese los datos para maricularse \n Numero de cedula: '))
            nombre = input ('Nombres completos: ')
            email = input ('Dirección de correo electronico:')
            carrera = input ('Carrera a la que va a matriculase:')
            ciclo = int(input('Ciclo al que deseas matricularte:'))
            base.Matric_est(cedula, nombre, email, carrera, ciclo)
        if op == 2:
            cedula = input('Ingrese el numero de C.I. a consultar: ')
            base.visualizar_datos(cedula)
        if op == 3:
            id = input('Ingrese un numero de id a buscar: ')
            base.buscar_datos(id)
        if op == 4:
            id = input('Ingrese un numero de id a eliminar: ')
            base.eliminar_datos(id)
        '''else:
            print ('ingrese una opción correcta')'''